function [int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en, ...
          int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy,...
          int_nn_facet, int_nf_facet, int_ff_facet, mean_fn_facet, Ds_fn_facet] ...
          = FEoperators(varargin)
%         [int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en, ...
%          int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy,...
%          int_nn_facet, int_nf_facet, int_ff_facet, mean_fn_facet, Ds_fn_facet] ...
%          = FEoperators(mesh, opts);
% or
%          = FEoperators(connect, coords, connect_facet, bmark_facet, opts)
% or
%          = FEoperators()
%
% Sets up integration, averaging and differentiation operators for piecewise linear finite
% elements on simplices in 1 or 2 dimensions includes operators defined on a
% one-dimensional network inside the domain (make only proper sense for the 2D case).
%
% If called with no arguments it returns the default option structure.
%
% Example usage for residual calculation of Poisson equation:
%     res =  (Dx_en' *int_ee* Dx_en ...
%           + Dy_en' *int_ee* Dy_en) * phi ...
%           - int_nn*f - int_nf_bdy * neumann;
% 
% INPUT
% 1) Input data structure 'mesh' must contain the fields
%   n_nodes:          number of nodes
%   n_elements:       number of elements
%   n_facets:         number of network elements/facets
%   nodes:            dimension-by-n_nodes array of Cartesian coordinates of nodes
%   connect:          n_elements-by-(dimension+1) array of vertex indices for each element
%   connect_facet:    n_facets-by-2 array of vertex for each network element
%   bmark_facet:     boundary marks for facets: 0 inside, odd on Dirichlet, even on Neumann
% 2)
%   connect:          n_elements-by-(dimension+1) array of vertex indices for each element
%   coords:           dimension-by-n_nodes array of Cartesian coordinates of nodes
%   connect_facet:    n_facets-by-2 array of vertex for each network element
%   bmark_facet:     boundary marks for facets: 0 inside, odd on Dirichlet, even on Neumann
%
% OUTPUT
%
% n --> node
% e --> element  (triangles 2D or lines in 1D)
% f --> facet    (lines 2D or points in 1D)
%
% Normal element operators:
%   int_nn: f.'*int_nn*g integrates the product of two functions, both defined by linear
%           interpolation between nodal values f and g
%   int_ne: f.'*int_ne*h integrates the product of two functions, one defined by
%           interpolation between nodal values f, the other by piecewise constant
%           values h on elements
%   int_ee: k.'*int_ee*h integrates the product of two functions, each piecewise
%           constant values k and h on elements
%   mean_en:    mean_en*f computes mean over each element of a function defined by
%               linear interpolation between nodal values f
%   Dx_en:  Dx_en*f computes on each element the x-derivative of  a function defined by
%           linear interpolation between nodal values f
%   Dy_en: defined analogously.
%
% Boundary facet operators
%   int_nn_bdy: f.'*int_nn_bdy*g integrates over the boundary the product functions, both defined by linear
%               interpolation between nodal values f and g
%   int_nf_bdy: f.'*int_nf_bdy*h integrates over the boundary the product of two functions, one defined by
%               interpolation between nodal values f, the other by piecewise constant
%               values h on facets
%   int_ff_bdy: k.'*int_ee*h integrates over the boundary the product of two functions, each piecewise
%               constant values k and h on boundary facets
%   mean_fn_bdy:    mean_fn_bdy*f computes mean over each boundary facet of a function defined by
%                   linear interpolation between nodal values f
%  Note, the facet part of these is only defined on Neumann facets
%
% Network operators (on facets in 2D, in 1D they are identical to the normal operators):
%   int_nn_facet:  f.'*int_nn_facet*g integrates over all facets the product functions, both defined by linear
%                  interpolation between nodal values f and g
%   int_nf_facet:  f.'*int_nf_facet*h integrates over all facets the product of two functions, one defined by
%                  interpolation between nodal values f, the other by piecewise constant values h on facets
%   int_ff_facet:  k.'*int_ff_facet*h integrates over all facets the product of two functions, each piecewise
%                  constant values k and h on facets
%   mean_fn_facet: mean_fn_facet*f computes mean over all facets of a function defined by
%                  inear interpolation between nodal values f
%   Ds_fn_facet:   Ds_fn_facet*f computes on each facet the arclength derivative of a function defined by
%                  linear interpolation between nodal values f
%
% Fields of optional argument opts:
%   trapezoidal:    flag to compute int_nn by composite trapezoidal rule
%                   (yields a sparser int_nn, but is a lower order intergral)
%   trapezoidal_facet:    flag to compute int_nn_facet by composite trapezoidal rule
%                       (yields a sparser int_nn_facet, but is a lower order intergral)
%
% TODO:
% - add normal derivative to facets to allow discontinuous Galerkin methods
%
% References:
%  ESW: Finite elements and fast iterative solvers; Elman, Silvester, Wathen

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

defopts.trapezoidal = false;
defopts.trapezoidal_facet = false;
defopts.trapezoidal_bdy = false;

if nargin==0 % return default options
    int_nn = defopts;
    return 
elseif nargin==1 || nargin==2 % a mesh-structure
    mesh = varargin{1};
    if nargin==2
        opts = varargin{2};
    else
        opts = defopts;
    end
    % setup
    n_nodes = mesh.n_nodes;                 %number of nodes
    n_elements = mesh.n_elements;           %number of elements
    x = mesh.nodes;                         %array of node locations, size n_elements-by-dimension
    connect = mesh.connect;                 %n_elements-by-(dimension+1) array listing the nodes connect(i,1), connect(i,2) etc that are vertices of each element labelled i

    if isfield(mesh, 'n_facets') % more modern mesh-structure
        n_facets = mesh.n_facets;
        connect_facet = mesh.connect_facet;
        % dimension agnostic setup
        neumann_facets = mesh.bmark_facet>0 & ~logical(mod(mesh.bmark_facet,2)); %number of boundary faces with Neuman BCs: bmark_facet is even
    else % legacy mesh-structure as used by mattri
        n_facets = mesh.n_edges;
        connect_facet = mesh.connect_edge;
        % dimension agnostic setup
        neumann_facets = mesh.bmark_edge>0 & ~logical(mod(mesh.bmark_edge,2)); %number of boundary faces with Neuman BCs: bmark_facet is even
    end 
else % separate inputs
    connect = varargin{1};
    x = varargin{2};
    connect_facet = varargin{3};
    bmark_facet = varargin{4};
    if nargin==5
        opts = varargin{5};
    else
        opts = defopts;
    end

    n_nodes = size(x,1);                 %number of nodes
    n_elements = size(connect,1);           %number of elements
    n_facets = size(connect_facet,1);      %number of facets
    neumann_facets = bmark_facet>0 & ~logical(mod(bmark_facet,2)); %number of boundary faces with Neuman BCs: bmark_facet is even
end

dimension = size(connect,2)-1;     %dimensionality of domain

n_elements_bdy = sum(neumann_facets);
elements = (1:n_elements)';
elements_bdy = (1:n_elements_bdy)';

if dimension == 1
    %operators defined on entire mesh
    connect1 = connect(:,1);
    connect2 = connect(:,2);
    dx_connect = x(connect2)-x(connect1); % Jacobian
    abs_dx_connect = abs(dx_connect);   % element size
    if opts.trapezoidal 
        %composite trapezoidal rule
        int_nn = sparse([connect1; connect2], [connect1; connect2], [1/2*abs_dx_connect; 1/2*abs_dx_connect], n_nodes,n_nodes);
    else 
        %exact quadrature
        int_nn = sparse([connect1;           connect1;           connect2;           connect2], ...
                        [connect1;           connect2;           connect1;           connect2], ...
                        [1/3*abs_dx_connect; 1/6*abs_dx_connect; 1/6*abs_dx_connect; 1/3*abs_dx_connect], n_nodes,n_nodes);
    end
    int_ne =  sparse([connect1; connect2], [elements; elements], [1/2*abs_dx_connect; 1/2*abs_dx_connect], n_nodes,n_elements);
    int_ee =  sparse(elements, elements, abs_dx_connect, n_elements, n_elements);
    mean_en = sparse([elements; elements], [connect1; connect2], 1/2*ones(2*n_elements,1), n_elements,n_nodes);     
    Dx_en =   sparse([elements; elements], [connect1; connect2], [-1./dx_connect; 1./dx_connect], n_elements,n_nodes);
    Dy_en =   sparse(n_elements, n_nodes);  %zero matrix in 1D
    
    % boundary operators
    connect_bdy1 = find(neumann_facets);
    int_nn_bdy  = sparse(connect_bdy1, connect_bdy1, ones(size(connect_bdy1)), n_nodes,n_nodes);
    int_nf_bdy  = sparse(connect_bdy1, elements_bdy, ones(size(connect_bdy1)), n_nodes,n_elements_bdy);
    int_ff_bdy  = sparse(elements_bdy, elements_bdy, ones(n_elements_bdy,1),   n_elements_bdy,n_elements_bdy);
    mean_fn_bdy = sparse(elements_bdy, connect_bdy1, ones(size(connect_bdy1)), n_elements_bdy,n_nodes);
    
    % network operators, in 1D equivalent to element operators
    if nargout>10
        if ~all(connect_facet==connect)
            error('For facets on 1D: mesh.connect==mesh.connect_facet')
        end
        int_nn_facet = int_nn;
        int_nf_facet = int_ne;
        int_ee_facet = int_ee;
        mean_fn_facet =  mean_en;
        Ds_fn_facet = Dx_en;
    end
elseif dimension == 2
    % operators defined on entire mesh
    connect1 = connect(:,1);
    connect2 = connect(:,2);
    connect3 = connect(:,3);
    x1 = x(connect1,1); x2 = x(connect2,1); x3 = x(connect3,1);
    y1 = x(connect1,2); y2 = x(connect2,2); y3 = x(connect3,2);
    Jacobian = (x2-x1).*(y3-y1) - (x3-x1).*(y2-y1);   % Jacobian of canonical transformation
    abs_dS_connect = abs(Jacobian)/2;   % the area of each element
    if opts.trapezoidal
        %composite trapezoidal rule
        int_nn = sparse([connect1; connect2; connect3], [connect1; connect2; connect3],...
                        [1/3*abs_dS_connect; 1/3*abs_dS_connect; 1/3*abs_dS_connect], n_nodes,n_nodes);
    else 
        %exact quadrature
        int_nn = sparse([connect1; connect1; connect1; connect2; connect2; connect2; connect3; connect3; connect3],...
                        [connect1; connect2; connect3; connect1; connect2; connect3; connect1; connect2; connect3],...
                        [abs_dS_connect/6; abs_dS_connect/12; abs_dS_connect/12; abs_dS_connect/12; abs_dS_connect/6; ...
                                           abs_dS_connect/12; abs_dS_connect/12; abs_dS_connect/12; abs_dS_connect/6],...
                        n_nodes,n_nodes);
    end
    int_ne = sparse([connect1; connect2; connect3], [elements; elements; elements], [1/3*abs_dS_connect; 1/3*abs_dS_connect; 1/3*abs_dS_connect], n_nodes,n_elements);
    int_ee = sparse(elements, elements, abs_dS_connect, n_elements,n_elements);
    mean_en =  sparse([elements; elements; elements], [connect1; connect2; connect3], 1/3*ones(3*n_elements,1), n_elements,n_nodes); 

    Dx_en = sparse([elements; elements; elements], [connect1; connect2; connect3], [(y2-y3)./Jacobian; (y3-y1)./Jacobian; (y1-y2)./Jacobian], n_elements,n_nodes);
    Dy_en = sparse([elements; elements; elements], [connect1; connect2; connect3], [(x3-x2)./Jacobian; (x1-x3)./Jacobian; (x2-x1)./Jacobian], n_elements,n_nodes);

    % boundary operators
    connect_bdy = connect_facet(neumann_facets,:); 
    connect_bdy1 = connect_bdy(:,1);
    connect_bdy2 = connect_bdy(:,2);
    dl_bdy = ((x(connect_bdy2,1)-x(connect_bdy1,1)).^2+(x(connect_bdy2,2)-x(connect_bdy1,2)).^2).^(1/2); % facet size, aka facet-Jacobian
    if opts.trapezoidal_bdy
        %composite trapezoidal rule
        int_nn_bdy = sparse([connect_bdy1; connect_bdy2],[connect_bdy1; connect_bdy2],[1/2*dl_bdy; 1/2*dl_bdy],n_nodes,n_nodes);
    else
        %exact quadrature
        int_nn_bdy = sparse([connect_bdy1; connect_bdy1; connect_bdy2; connect_bdy2], [connect_bdy1; connect_bdy2; connect_bdy1; connect_bdy2],...
                            [1/3*dl_bdy; 1/6*dl_bdy; 1/6*dl_bdy; 1/3*dl_bdy], n_nodes,n_nodes);
    end
    int_nf_bdy =  sparse([connect_bdy1; connect_bdy2], [elements_bdy; elements_bdy], [1/2*dl_bdy; 1/2*dl_bdy], n_nodes,n_elements_bdy);
    int_ff_bdy =  sparse(elements_bdy, elements_bdy, dl_bdy, n_elements_bdy,n_elements_bdy);
    mean_fn_bdy = sparse([elements_bdy; elements_bdy], [connect_bdy1; connect_bdy2], 1/2*ones(2*n_elements_bdy,1), n_elements_bdy,n_nodes);
    
    % Network operators
    if nargout>10
        connect_facet1 = connect_facet(:,1);
        connect_facet2 = connect_facet(:,2);
        facets = (1:n_facets)';
        dl_facet = ((x(connect_facet2,1)-x(connect_facet1,1)).^2+(x(connect_facet2,2)-x(connect_facet1,2)).^2).^(1/2);
        if opts.trapezoidal_facet
            %composite trapezoidal rule
            int_nn_facet = sparse([connect_facet1; connect_facet2], [connect_facet1; connect_facet2],...
                                 [1/2*dl_facet; 1/2*dl_facet], n_nodes,n_nodes);
        else
            %exact quadrature
            int_nn_facet = sparse([connect_facet1; connect_facet1; connect_facet2; connect_facet2], ...
                                  [connect_facet1; connect_facet2; connect_facet1; connect_facet2],...
                                  [1/3*dl_facet;    1/6*dl_facet;    1/6*dl_facet;    1/3*dl_facet], n_nodes,n_nodes);
        end
        int_nf_facet =  sparse([connect_facet1; connect_facet2], [facets; facets], [1/2*dl_facet; 1/2*dl_facet], n_nodes,n_facets);
        int_ff_facet =  sparse(facets, facets, dl_facet, n_facets,n_facets);
        mean_fn_facet = sparse([facets; facets], [connect_facet1; connect_facet2], 1/2*ones(2*n_facets,1), n_facets,n_nodes);
        Ds_fn_facet =   sparse([facets; facets], [connect_facet1; connect_facet2], [-1./dl_facet; 1./dl_facet], n_facets,n_nodes);
        %        Dn_fe_facet =   sparse([facets; facets], [connect_facet_el1; connect_facet_el2], [???; ???], n_facets,n_elements);
    end
end
