# sparseFEM

This Matlab/Octave package implements the finite element method (FEM)
in 1 and 2 dimensions.  It works by defining global FE operators as
sparse matrices.  The FE assembly then becomes sparse matrix
multiplications.  In Matlab/Octave this maybe the fastest way to do
finite elements as loops are slow.  In fact a comparison shows that
this method is faster than what FEniCS can do!

Finite element assembly can be written very succinctly in this
framework, *no loops required*!  Get the global FE operators with the
core function of the package `FEoperators`:

```
[int_nn, ~, int_ee, ~, Dx_en, Dy_en, ~, int_nf_bdy] = FEoperators(mesh);
```

The assembly of the stiffness matrix for a Laplace operator can be
written as

```
stiff = Dx_en.'*int_ee *Dx_en + Dy_en.'*int_ee *Dy_en;
```

and the assembly for the force vector as

```
force = int_nn * f + int_nf_bdy * neumann;
```

Then solve with Matlab's `\`:

```
solution = stiff\force;
```

This ignores the issue of enforcement of Dirichlet boundary
conditions, see `examples/poisson.m` for a full example.

Call `help FEoperators` for documentation.

## Comparison to FEniCS

Somewhat surprisingly, sparseFEM is faster than FEniCS by quite a bit!
The following shows a plot of how much faster sparseFEM is than
FEniCS:  Including the generation of the FE operators, sparseFEM is
about 1.5x faster.  However, for a mesh the operators only need to be
made once and after that the assembly is ~15x faster than FEniCS! But
of course, FEniCS has to offer many other benefits.

![Relative speed advantage of sparseFEM over FEniCS](https://bytebucket.org/maurow/sparsefem/raw/master/fenics_comp/assembly_comp_rel.png)

The code to run the comparison is in `fenics_comp/`.

## Dependencies
Optional:

- use [mattri](https://bitbucket.org/maurow/mattri) or
  [distmesh](http://persson.berkeley.edu/distmesh/) to generate 2D
  meshes.

## Examples & demo
In directory `examples/` run `demo.m` and look at the m-files.

## Authors and licence
This package was written by

Christian Schoof, Ian J Hewitt & Mauro A Werder

Licensed under a BSD 2-Clause License, see LICENCE file.

