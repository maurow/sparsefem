% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

disp(' ')
disp('Solving Poisson equation in 1D')
disp(' ')
dims = 1;
poisson;
input('Press enter to continue', 's')
close all

disp('----')
disp(' ')
disp('Solving Poisson equation in 2D')
disp(' ')
dims = 2;
poisson;
input('Press enter to continue', 's')
close all

disp(' ')
disp('Convergence rate of Poisson equation in 1D')
disp(' ')
dims = 1;
poisson_convergence;
input('Press enter to continue', 's')
close all

disp(' ')
disp('Convergence rate of Poisson equation in 2D')
disp(' ')
dims = 2;
poisson_convergence;
input('Press enter to continue', 's')
close all

disp('----')
disp(' ')
disp('Solving shallow ice equation in 1D')
disp(' ')
dims = 1;
shallow_ice;
input('Press enter to continue', 's')
close all

disp('----')
disp(' ')
disp('Solving shallow ice equation in 2D')
disp(' ')
dims = 2;
shallow_ice;
input('Press enter to continue', 's')
close all

