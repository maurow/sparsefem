% This solves the Poisson equation in 1D or 2D (set dims==1 or ==2)
%
% - d/dx( k dphi/dx)) = f
%
% k = 1, f = 10
%
% with boundary conditions:
% - 1D: dphi(0)/dn = bcn, phi(end)=x
% - 2D: phi = x everywhere, except dphi/dn = bcn at x=0
%   with bcn=1
%
% Weak form:
% int( grad theta . k grad phi) = int( theta f) + int_b(theta k dphi/dn)

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

addpath ..
addpath ../../mattri

if ~exist('dims', 'var')
    dims = 2; % choose 1D or 2D
end
% whether to use the mattri meshing package or the built-in rectangular mesh maker.  The
% former can make more complex meshes.  Also consider distmesh.
if ~exist('usemattri', 'var') || isempty(usemattri)
    usemattri = false;  
end

if dims==1
    %% 1D mesh
    len = 100;
    x0 = 0;
    x1 = 1;
    dx = (x1-x0)/len;
    nodes = linspace(x0,x1,len)' + rand(len,1)*0.4*dx;   % make irregular mesh

    tic
    mesh = make_1D_mesh(nodes);
    mesh.bmark(1) = 2;
    mesh.bmark_facets(1) = 2;
    mesh.bmark(end) = 1;
    mesh.bmark_facets(end) = 1;
    tt=toc;
    disp(['Time to create the mesh: ', num2str(tt)])
else
    %% 2D mesh
    % make a bounding polygon
    boundary_xy = [0,0;
                   1,0;
                   0.5,0.5;
                   1,1;
                   0,1;];  % do not close the loop!

    bmark = [1;1;1;1;1];        % give bmark==1 to all nodes to specify Dirichlet
    bmark_edge = [1;1;1;1;2];   % give a bmark_edge==1 except at x=0 where Neumann BC
    maxarea = 0.001;            % maximal area of triangles

    tic
    if usemattri
        if exist('process_Triangle_output')==2
            mesh = make_2D_mesh(boundary_xy, bmark, bmark_edge, maxarea);
        else
            disp(['mattri meshing package not found.  Install and set matlab path.'])
            disp(['https://bitbucket.org/maurow/mattri'])
            disp(['...loading example mesh from file.'])
            disp(' ')
            load boxmesh.mat  
        end
    else        
        xrange = [boundary_xy(1,1),boundary_xy(2,1)];
        yrange = [boundary_xy(1,2),boundary_xy(end,2)];
% $$$         dx = sqrt(maxarea)*2;
% $$$         nx = round(diff(xrange)/dx);
% $$$         ny = round(diff(yrange)/dx);
        nx = 4
        ny = 7
        mesh = make_2D_boxmesh(nx, ny, xrange, yrange, bmark(1:4), bmark_edge(2:end));
    end
        
    tt=toc;
    disp(['Time to create the mesh: ', num2str(tt)])
% $$$ figure;
% $$$ mesh_plot_tri(gca, mesh)
% $$$ axis tight
% $$$ title('The mesh')

end

%% FEM

k = 1; % this could be a n_elements x n_elements matrix
f = 10 * mesh.nodes(:,1);
bcn = 1;  % dphi/dn at Neumann boundary edges

% FEM setup
[int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en, ...
 int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy] = FEoperators(mesh);

% active nodes, i.e. not Dirichlet nodes
anodes = mod(mesh.bmark,2)==0; 
dirinodes = ~anodes;
%number of boundary faces with Neumann BCs: bmark_facet is even
neumann_facets = mesh.bmark_facet>0 & mod(mesh.bmark_facet,2)==0; 

% BC
diri = mesh.nodes(dirinodes,1); 
phi = zeros(mesh.n_nodes,1);
phi(dirinodes) = diri;
neumann = bcn * ones(sum(neumann_facets),1);

tic
stiff =   Dx_en(:,anodes).'*int_ee* k *Dx_en(:,anodes) ...
      + Dy_en(:,anodes).'*int_ee* k *Dy_en(:,anodes);
% do BCs by adding contribution of Dirichlet nodes to force vector:
force = int_nn(anodes,:) * f + k * int_nf_bdy(anodes,:) * neumann ...
      - Dx_en(:,anodes).'*int_ee* k *(Dx_en(:,dirinodes)*phi(dirinodes))...
      - Dy_en(:,anodes).'*int_ee* k *(Dy_en(:,dirinodes)*phi(dirinodes));

tt=toc;
disp(['Time for assembly: ', num2str(tt)])

% solve
tic
phi(anodes) = stiff\force;
tt=toc;
disp(['Time for linear solve: ', num2str(tt)])

% Aside: the residual can be written more cleanly:
res =  (Dx_en.'*int_ee* k * Dx_en...
      + Dy_en.'*int_ee* k * Dy_en) * phi ...
      - int_nn*f - int_nf_bdy * neumann;

% calculate flux by projection, i.e. solve w = grad phi for w
stiff = int_nn;
force = int_ne * Dx_en * phi;
qx = stiff\force;
force = int_ne * Dy_en * phi;
qy = stiff\force;
q = sqrt(qx.^2+qy.^2);


%% the result
figure
if dims==1
    plot(mesh.nodes, phi)
else
    patch_props = {'EdgeColor', 'none', 'FaceColor', 'interp'};
    trisurf(mesh.connect, mesh.nodes(:,1), mesh.nodes(:,2), phi, patch_props{:});
end
title('The solution')
axis tight
