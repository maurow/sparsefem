% This solves the Poisson equation in 2D
%
% - d/dx( k dphi/dx)) = f
%
% with boundary conditions phi(0) = 1, phi(end)=1
%
% Weak form:
% int( grad theta . k grad phi) = int( theta f) + int_b(theta dphi/dn)

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

addpath ..
addpath ../../mattri

if ~exist('dims', 'var')
    dims = 1; % 1 or 2
end


%% Manufactured solution
manusol = @(x,y) cos(x).*cos(y);
% forcing which produces the manufactured solution
force_fn = @(x,y) 2*cos(x).*cos(y); % if dims==1 -> divided by two further down

norminf = [];
n_nodes = [];
edge_length = [];

if dims==1
    ns = 2.^(2:17);
else
    ns = 2.^(2:10);
end
% run with different mesh sizes
for n= ns
    %% mesh
    if dims==1
        %% 1D mesh
        x0 = -pi/2;
        x1 = pi/2;
        rand_fac = 0;
        mesh = make_1D_mesh(x0, x1, n, rand_fac);
    else
        %% 2D mesh
        nx = n; ny = n;
        bmark = ones(4,1); bmark_edge = ones(4,1);
        mesh = make_2D_boxmesh(nx, ny, [-pi,pi]/2, [-pi,pi]/2, bmark, bmark_edge);
    end
    
    %% FEM
    k = 1;
    if dims==1
        solm = manusol(mesh.nodes(:,1),0);
        f = force_fn(mesh.nodes(:,1),0)/2;
    else
        solm = manusol(mesh.nodes(:,1),mesh.nodes(:,2));
        f = force_fn(mesh.nodes(:,1),mesh.nodes(:,2));
    end

    % FEM setup
    [int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en, ...
     int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy] = FEoperators(mesh);

    % active nodes, i.e. not Dirichlet nodes
    anodes = mod(mesh.bmark,2)==0; 
    dirinodes = ~anodes;
    %number of boundary faces with Neumann BCs: bmark_facet is even
    neumann_facets = mesh.bmark_facet>0 & mod(mesh.bmark_facet,2)==0; 

    % BC
    diri = 0;
    phi = zeros(mesh.n_nodes,1);
    phi(dirinodes) = diri;
    neumann = 0* ones(sum(neumann_facets),1);

    % do BCs by adding contribution of Dirichlet nodes to force (forcing):

    stiff =   Dx_en(:,anodes).'*int_ee* k *Dx_en(:,anodes) ...
            + Dy_en(:,anodes).'*int_ee* k *Dy_en(:,anodes);
    force = int_nn(anodes,:) * f + int_nf_bdy(anodes,:) * neumann ...
          - Dx_en(:,anodes).'*int_ee* k *(Dx_en(:,dirinodes)*phi(dirinodes))...
          - Dy_en(:,anodes).'*int_ee* k *(Dy_en(:,dirinodes)*phi(dirinodes));

    % solve
    phi(anodes) = stiff\force;
    
% $$$     %% solve with residual and fsolve
% $$$     resi = @(phi) poisson_residual(phi, k, f, Dx_en, Dy_en, int_ee, int_nn, mean_en, dirinodes, find(dirinodes)*0+diri);
% $$$     opts = optimoptions('fsolve', 'TolX', 1e-12);
% $$$     phi_res = fsolve(resi, phi*0, opts);
% $$$     norminf(end+1) = norm(solm-phi_res, Inf);

    norminf(end+1) = norm(solm-phi, Inf);
    n_nodes(end+1) = mesh.n_nodes;
end
%% the result

figure
if dims==1
    loglog(n_nodes, norminf, '.-')
    xlabel('number of nodes')
    slope_h   = median(diff(log10(norminf))./diff(log10(n_nodes)))
else
    loglog(sqrt(n_nodes), norminf, '.-')
    xlabel('sqrt(number of nodes)')
    slope_h   = median(diff(log10(norminf))./diff(log10(sqrt(n_nodes))))
end    
axis tight
ylabel('max error')
title('convergence rate')

