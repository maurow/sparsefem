% This example is as poisson.m but uses distmesh as mesh generator.  Download distmesh
% from:
% http://persson.berkeley.edu/distmesh/

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

%addpath path_to_distmesh

if exist('OCTAVE_VERSION')
    disp('This example does not run in Octave as distmesh does not run in Octave.')
    return
end

addpath ..

dims = 2;
boundary_xy = [0,0;
               1,0;
               0.5,0.5;
               1,1;
               0,1;
              0,0];  %  CLOSE THE LOOP! (for distmesh)

edgelength = 0.05;            % maximal edge length of triangles

tic
[nodes,connect]=distmesh2d(@dpoly,@huniform, edgelength,[0,0; 1,1],boundary_xy,boundary_xy); 
%% process mesh as in make_2D_boxmesh.m, see that file for comments
n_nodes = size(nodes,1);
% make all edges
connect_facet_dup = [connect(:, [1,2]); connect(:, [1,3]); connect(:, [2,3])];
connect_facet_dup = sort(connect_facet_dup, 2);
[connect_facet, ~, IC] = unique(connect_facet_dup, 'rows');
n_facets = size(connect_facet,1);
facet_midpoints = (nodes(connect_facet(:,1),:) + nodes(connect_facet(:,2),:))/2;

hi = histc(IC, 1:n_facets);
facets_on_boundary = find(hi==1);
nodes_on_boundary = connect_facet(facets_on_boundary,:);
nodes_on_boundary = unique(nodes_on_boundary(:));
% bmarks: ==1 except at x=0 ==2
small = edgelength/100;
bmark = zeros(n_nodes,1);
bmark(nodes_on_boundary) = 1; % all ==1
bmark(nodes(:,1)<small) = 2; % except x=0
bmark(nodes(:,2)<small) = 1; % but not (0,0)
bmark(nodes(:,2)>1-small) = 1; % but not (0,1)
bmark_facet = zeros(n_facets,1);
bmark_facet(facets_on_boundary) = 1;
bmark_facet(facet_midpoints(facets_on_boundary,1)<small) = 2;

tt=toc;
disp(['Time to create the mesh: ', num2str(tt)])
close();

%% FEM
k = 1;
f = 10* nodes(:,1);

% FEM setup
[int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en, ...
 int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy] = ...
    FEoperators(connect, nodes, connect_facet, bmark_facet);

% active nodes, i.e. not Dirichlet nodes
anodes = mod(bmark,2)==0; 
dirinodes = ~anodes;
%number of boundary faces with Neumann BCs: bmark_facet is even
neumann_facets = bmark_facet>0 & mod(bmark_facet,2)==0; 

% BC
diri = nodes(dirinodes,1); 
phi = zeros(n_nodes,1);
phi(dirinodes) = diri;
neumann = 0* ones(sum(neumann_facets),1);

% do BCs by adding contribution of Dirichlet nodes to force (forcing):
tic
stiff =   Dx_en(:,anodes).'*int_ee* k *Dx_en(:,anodes) ...
      + Dy_en(:,anodes).'*int_ee* k *Dy_en(:,anodes);
force = int_nn(anodes,:) * f + int_nf_bdy(anodes,:) * neumann ...
      - Dx_en(:,anodes).'*int_ee* k *(Dx_en(:,dirinodes)*phi(dirinodes))...
      - Dy_en(:,anodes).'*int_ee* k *(Dy_en(:,dirinodes)*phi(dirinodes));

tt=toc;
disp(['Time for assembly: ', num2str(tt)])

% solve
tic
phi(anodes) = stiff\force;
tt=toc;
disp(['Time for linear solve: ', num2str(tt)])

% Aside that the residual can be written more cleanly:
res =  (Dx_en.'*int_ee* k * Dx_en...
      + Dy_en.'*int_ee* k * Dy_en) * phi ...
      - int_nn*f - int_nf_bdy * neumann;

% calculate flux by projection, i.e. solve w = grad phi for w
stiff = int_nn;
force = int_ne * Dx_en * phi;
qx = stiff\force;
force = int_ne * Dy_en * phi;
qy = stiff\force;
q = sqrt(qx.^2+qy.^2);


%% the result
figure
patch_props = {'EdgeColor', 'none', 'FaceColor', 'interp'};
trisurf(connect, nodes(:,1), nodes(:,2), phi, patch_props{:});
title('The solution')
axis tight
