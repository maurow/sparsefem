function res = poisson_residual(phi, k, f, Dx_en, Dy_en, int_ee, int_nn, mean_en, dirinodes, dirivals)
% res = poisson_residual(phi, k, f, Dx_en, Dy_en, int_ee, int_nn, mean_en, dirinodes, dirivals)
%
% Calculates the residual for the Poisson equation.

%  Boundary conditions by projection method:
%  
%  Let Ri be the projector into the space with (prescribed) inhomogeneous
%  Dirichlet values, Ri0 the projector into the space with homogeneous
%  boundary values (i.e. annihilator of the Dirichlet nodes), and Rd the
%  annihilator of the interior values (preserving the Dirichlet values).
%  Then to evaluate the residual f, you do
%  
%    F(x) = Ri0 f(Ri x) + scale * (Rd x - x_D)
%  
%  where f(.) is the usual elementwise physics, scale=1 or some value
%  similar to the local viscosity (can always be 1 except for geometric
%  multigrid or very poorly scaled problems), and F(.) is the discrete
%  residual including Dirichlet conditions.  The Jacobian of this
%  formulation will have no coupling between the Dirichlet nodes and
%  non-Dirichlet nodes (i.e. corresponding rows and columns will be zero
%  except for scale on the diagonal), and the Dirichlet conditions will be
%  exactly satisfied after one iteration.

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

% do Ri
phidiri = phi(dirinodes);
phi(dirinodes) = dirivals;

% evaluate residual
grad_phix = Dx_en*phi;
grad_phiy = Dy_en*phi;
abs_grad_phi = sqrt(grad_phix.^2 + grad_phiy.^2);
phi_mean = mean_en*phi; % phi on elements

res =  (Dx_en.'*int_ee* (k .* grad_phix) ...
      + Dy_en.'*int_ee* (k .* grad_phiy))   ...
      - int_nn*f;

% do Ri0 & do: scale * (Rd x - x_D)
scale = 1;
res(dirinodes) =  scale *(phidiri-dirivals);
