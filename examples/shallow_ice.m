% Solving a nonlinear PDE, the steady shallow ice equation (a non-linear diffusion
% equation) and compares it to analytic solutions given in [1].  The Picard iteration
% needs to be under-relaxed to work, the scheme used is described in [2].  (Can also be
% solved using fsolve)
%
% div Q = a
% Q = - gamma/(n+2) h^(n+2) * |grad h|^(n-1) grad h
%
% Rescaling x->Lx, y->Ly, h->Zh, a->Aa gives:
% - div (h^(n+2) |grad h|^(n-1) grad h) = a
%
% with ( A(n+2) L^(n+1) )/ (gamma Z^(2n+2)) = 1
%
% References:
% [1] https://www.cs.uaf.edu/~bueler/steadyiso.pdf
% [2] RCA Hindmarsh, AJ Payne - Annals of Glaciology, 1996, page 84

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

addpath ..
addpath ../../mattri

if ~exist('dims', 'var')
    dims = 2; % 1 or 2
end

L = 1500e3/2; % domain radius
n = 3;  % Glen's exponent
g = 9.81; % grav. accel.
rho = 910.0; % density of ice
secpera = 365*24*3600; % sec. per year
A0 = 1.0e-16 / secpera;  % ice flow constant
gamma = 2 * A0 * (rho * g)^n;

hmax = 3500;  % maximal ice thickness

% scaling
Z = hmax;
A = gamma * Z^(2*n+2)/((n+2)*L^(n+1));

regularisation = 100*eps;

tol = 1e-3;
maxiter = 1000;

if dims==1
    %% 1D mesh
    len = 100;
    x0 = -L/L;
    x1 = L/L;
    dx = (x1-x0)/len;
    nodes = linspace(x0,x1,len)';
    rand_fac = 0.;   % make irregular mesh, rand_fac<0.5
    nodes(2:end-1) = nodes(2:end-1) + rand(len-2,1)*rand_fac*dx; 

    tic
    mesh = make_1D_mesh(nodes);
    mesh.bmark([1, end]) = 1;
    mesh.bmark_facet([1, end]) = 1;
    tt=toc;
    disp(['Time to create the mesh: ', num2str(tt)])

    % FEM setup
    [int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en, ...
     int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy] = FEoperators(mesh);
 
    % Get forcing (accumulation) from analytic solution:
    [h_fn, a_fn, Q_fn] = shallow_ice_analytic(dims, L, n, hmax, gamma);
    aana = a_fn(L*mesh.nodes);
    hana = h_fn(L*mesh.nodes); % analytic solution which goes with a
    Qana = Q_fn(L*mesh.nodes); % analytic flux Q
    Qana_mean = mean_en*Qana;

else
    %% 2D mesh
    % make a bounding polygon
    %maxarea = 10e3^2;            % maximal area of triangles
    maxarea = (L/L)^2*pi/1000;            % maximal area of triangles
    len = sqrt(2*maxarea);
    pieces = round(2*L/L*pi/len);
    ang = linspace(0,2*pi, pieces)';
    boundary_xy = L/L*[cos(ang(1:end-1)), sin(ang(1:end-1))];
    bmark = boundary_xy(:,1)*0 +1;
    bmark_edge = bmark;
    
    if exist('process_Triangle_output')==2
        tic
        mesh = make_2D_mesh(boundary_xy, bmark, bmark_edge, maxarea);
        tt=toc;
        disp(['Time to create the mesh: ', num2str(tt)])
    else
        disp(['mattri meshing package not found.  Install and set matlab path.'])
        disp(['https://bitbucket.org/maurow/mattri'])
        disp(['...loading example mesh from file.'])
        disp(' ')
        load circlemesh.mat  
    end

    % FEM setup
    [int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en, ...
     int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy] = FEoperators(mesh);
    
    % Get forcing (accumulation) from analytic solution:
    [h_fn, a_fn, Q_fn] = shallow_ice_analytic(dims, L, n, hmax, gamma);
    aana = a_fn(L*mesh.nodes(:,1), L*mesh.nodes(:,2));
    hana = h_fn(L*mesh.nodes(:,1), L*mesh.nodes(:,2)); % analytic solution which goes with a
    Qana = Q_fn(L*mesh.nodes(:,1), L*mesh.nodes(:,2)); % analytic flux Q
    Qana_mean = mean_en*Qana;

end
% scale analytic forcing:
a = aana/A;

% active nodes, i.e. not Dirichlet nodes
anodes = ~(mesh.bmark==1);
dirinodes = ~anodes;
%number of boundary faces with Neumann BCs: bmark_facet is even
neumann_facets = mesh.bmark_facet>0 & ~logical(mod(mesh.bmark_facet,2)); 

% BC
dirivals = find(dirinodes)*0;
h = zeros(mesh.n_nodes,1);
h(dirinodes) = dirivals;
neumann = 0* ones(sum(neumann_facets),1);

% IC: the solver needs a fairly good IC!
if dims==1
    h = cos(mesh.nodes*pi/2);
else
    r = sqrt(mesh.nodes(:,1).^2 + mesh.nodes(:,2).^2);
    h = cos(r*pi/2);
end

%% solve by a under-relaxed Picard iteration

% these stay constant during the iteration:
force_const = int_nn(anodes,:) * a + int_nf_bdy(anodes,:) * neumann;
stiffx_const = Dx_en(:,anodes).'*int_ee;
stiffy_const = Dy_en(:,anodes).'*int_ee;

delta_h_old = 1;
h_old = h;
for ii=1:maxiter
    % assemble 
    diffuse = shallow_ice_diffusifity(h, n, Dx_en, Dy_en, mean_en, regularisation);
    Qx = bsxfun(@times, diffuse, Dx_en(:,anodes));
    Qy = bsxfun(@times, diffuse, Dy_en(:,anodes));
    stiff = stiffx_const * Qx + stiffy_const * Qy;
    force = force_const - ...
           (stiffx_const *(diffuse .* (Dx_en(:,dirinodes)*h(dirinodes)))  ...
          + stiffy_const *(diffuse .* (Dy_en(:,dirinodes)*h(dirinodes))));
    % solve linear system
    h(anodes) = stiff\force;
    h(dirinodes) = dirivals;
    % first check whether we converged:
    delta_h = h-h_old;
    err = norm(delta_h, inf);
    if err < tol
        break
    end
    % RCA Hindmarsh, AJ Payne - Annals of Glaciology, 1996, page 84
    % have a clever way to under-relax the iteration:
    alpha = norm(delta_h-delta_h_old, 2)/norm(delta_h_old, 2);
    delta_h = delta_h/alpha;
    % update h
    h = h_old + delta_h;
    h_old = h;
    delta_h_old = delta_h;
end
if ii==maxiter
    disp(['NOT CONVERGED after ', num2str(ii), ' Picard iterations. Error = ', num2str(norm(h-h_old, inf))])
else
    disp(['Converged after ', num2str(ii), ' Picard iterations. Error = ', num2str(norm(h-h_old, inf))])
end

if dims==1
    figure
    plot(mesh.nodes*L, h*Z, '.-');
    title('Solution h (m)')
    figure
    plot(mesh.nodes*L, h*Z-hana, '.-');
else
    meshds = mesh;
    meshds.nodes = meshds.nodes*L;
    figure
    patch_props = {'EdgeColor', 'none', 'FaceColor', 'interp'};
    trisurf(meshds.connect, meshds.nodes(:,1), meshds.nodes(:,2), h*Z, patch_props{:});
    title('Solution h (m)')
    figure
    trisurf(meshds.connect, meshds.nodes(:,1), meshds.nodes(:,2), h*Z-hana, patch_props{:});
end
title('Absolute error in ice thickness (m)')
    
%% Solve using residual and fsolve
% $$$ resi = @(h) shallow_ice_residual(h, n, a, Dx_en, Dy_en, int_ee, int_nn, mean_en, dirinodes, dirivals, regularisation);
% $$$ opts = optimoptions('fsolve', 'TolX', 1e-3);
% $$$ h_res = fsolve(resi, hana*0.1, opts);
% $$$ norminf = norm(hana/Z-h_res, Inf)
% $$$ plot(h_res-hana/Z)
