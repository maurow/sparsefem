function [h_fn, a_fn, Q_fn] = shallow_ice_analytic(dims, L, n, hmax, gamma)
%  [h_fn, a_fn, Q_fn] = shallow_ice_analytic(dims, L, n, hmax, gamma)
%
% Analytic solution for the shallow ice equations. Follows Bueler:
% https://www.cs.uaf.edu/~bueler/steadyiso.pdf

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if dims==1 % example 3 from above paper
    if n~=3
        error('n==3 for analytic sol in dims==1');
    end
    small_L = 1e-4;
    C1 = 8/3*(5/gamma)^(1/3);
    alpha = (2*hmax^(8/3)/(C1*L))^3;
    a_fn = @(x, y)(aana1d(x, L, n, hmax, alpha, small_L));
    h_fn = @(x, y)(hana1d(x, L, n, hmax, small_L));
    Q_fn = @(x, y)(Qana1d(x, L, alpha, small_L));
else % This is example 4 from above paper
    C1 = (2+2/n)*( (n+2)/gamma )^(1/n);
    small_L = 1e-6;
    alpha = ((1+1/n) * hmax^(2+2/n)/(C1 *L *(1-1/n)))^n;
    a_fn = @(x, y) real(aana2d(x,y, L, n, hmax, alpha, small_L));
    h_fn = @(x, y) real(hana2d(x,y, L, n, hmax, small_L));
    Q_fn = @(x, y) real(Qana2d(x,y, L, n, alpha, small_L));
end

%% 1D
function h = hana1d(x, L,n, hmax, small_L)
    L = L*(1+small_L);
    x = abs(x);
    h = hmax * ( 1 + 2*x/L - 3/2*(x/L).^(4/3) + 3/2*((1-x/L).^(4/3) -1)).^(3/8);
end
function a = aana1d(x, L,n, hmax, alpha, small_L)
    origin = find(x==0);
    x(origin) = L;
    L = L*(1+small_L);
    x = abs(x);
    a = alpha/L *( (x/L).^(1/3) + (1-x/L).^(1/3) -1 ).^2 .* ...
        ( (x/L).^(-2/3) - (1-x/L).^(-2/3) );
    a(origin) = -a(origin);
end
function Q = Qana1d(x, L, alpha, small_L)
    L = L*(1+small_L);
    xs = x;
    x = abs(x);
    beta = L^(-1/3);
    Q = sign(xs).* alpha .* ( beta*x.^(1/3) + beta *(L-x).^(1/3) -1 ).^3;
end


%% 2D
function h = hana2d(x,y, L,n, hmax, small_L)
    L = L*(1+small_L);
    r = sqrt(x.^2+y.^2);
    h = (hmax * ( 1 - n/(n-1) * ( (r/L).^(1+1/n) - (1-r/L).^(1+1/n) + 1 - (1+1/n)*r/L) ).^(n/(2*n+2)));
end
function a = aana2d(x,y, L,n, hmax, alpha, small_L)
    L = L*(1+small_L);
    r = sqrt(x.^2+y.^2);
    a = (alpha./r .* ( (r/L).^(1/n) + (1-r/L).^(1/n) -1 ).^n ...
        + alpha/L * ( (r/L).^(1/n) + (1-r/L).^(1/n) -1 ).^(n-1) ...
          .* ( (r/L).^(1/n-1) - (1-r/L).^(1/n-1)));
end
function Q = Qana2d(x, y, L, n, alpha, small_L)
    L = L*(1+small_L);
    r = sqrt(x.^2+y.^2);
    Q = (alpha * ( (r/L).^(1/n) + (1-r/L).^(1/n) - 1).^n);
end

end