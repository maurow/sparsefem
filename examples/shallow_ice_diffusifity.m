function [diffuse, grad_hx, grad_hy] = shallow_ice_diffusifity(h, n, Dx_en, Dy_en, mean_en, reg)
% diffuse = shallow_ice_diffusifity(h, n, Dx_en, Dy_en, mean_en, reg)
%
% Calculates the scaled effective diffusifity on the elements:
%  h^(n+2) * |grad h + reg|^(n-1) 
%
% Adds a regularisation: reg

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

grad_hx = Dx_en*h;
grad_hy = Dy_en*h;
abs_grad_h = sqrt(grad_hx.^2 + grad_hy.^2);
h_mean = mean_en*h; % h on elements

diffuse = h_mean.^(n+2) .* (abs_grad_h + reg).^(n-1);

