% Run and plot comparison between FEniCS and sparseFEM

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

% run fencis:
if ~exist('fenics_dof.txt', 'file')
    disp('Run FEniCS version from command line: python poisson_fencis.py')
    input('Press any key once done.')
end

% read files
disp('Reading FEniCS result files.')
p.dof = (load('fenics_dof.txt'))';
p.assemblytime = (load('fenics_assembly.txt'))';
p.solvetime = (load('fenics_solve.txt'))';

% run matlab
disp('Running sparseFEM')
poisson_matlab;
m.dof = ns.*ns;
m.assemblytime = assemblytime;
m.setuptime = setuptime;
m.solvetime = solvetime;

% plot
figure()
loglog(p.dof, p.assemblytime , '.-')
hold
loglog(m.dof, m.assemblytime , 'g.-')
loglog(m.dof, m.assemblytime+m.setuptime , 'r.-')
legend('FEniCS assembly', 'sparseFEM assembly', 'sparseFEM assembly+setup .', 'location', 'northwest' )
ylabel('Wall clock time (s)')
xlabel('DOF')
title('Assembly time')
axis tight
print -dpng assembly_comp

figure()
semilogx(p.dof, p.assemblytime./(m.assemblytime+m.setuptime) , '.-')
hold
semilogx(p.dof, p.assemblytime./(m.assemblytime) , 'r.-')
ylabel('Relative speed')
xlabel('DOF')
title('Relative speed advantage of sparseFEM over FEniCS')
legend('FEniCS assembly / (sparseFEM assembly+setup) .', 'FEniCS assembly / (sparseFEM assembly)', 'location', 'northeast' )
axis tight
ylim([0,20])
print -dpng assembly_comp_rel

% $$$ figure()
% $$$ loglog(p.dof, p.solvetime , '.-')
% $$$ hold
% $$$ loglog(m.dof, m.solvetime , 'g.-')
% $$$ legend('FEniCS', 'sparseFEM (matlab)', 'location', 'northwest' )
% $$$ ylabel('Wall clock time (s)')
% $$$ xlabel('DOF')
% $$$ title('Linear solve time')
% $$$ axis tight
