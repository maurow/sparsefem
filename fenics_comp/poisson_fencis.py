#!/usr/bin/env python2
# This is modifed from one of the FEniCS/Dolphin examples to solve the Poisson equation in 2D.

# Dolphin and thus this file has the following licence:
#
# DOLFIN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DOLFIN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.


from dolfin import *
import numpy as np
import pylab as plt

parameters['form_compiler']['optimize'] = True
parameters['form_compiler']['cpp_optimize'] = True

def manusol_fn(xy):
    return np.cos(xy[:,0]) * np.cos(xy[:,1])

manu_ex = Expression("cos(x[0])*cos(x[1])")
el_order = 1

errs = []
errs_int = []
dof = []
assemblytime = []
solvetime = []

nm = 2**np.arange(1,11)
nm = np.hstack((nm[0], nm)) # do one warm-up loop
counter = 0
for n in nm:
    ni = int(n)
    # Create mesh and define function space
    mesh = RectangleMesh(-pi/2, -pi/2, pi/2, pi/2, ni,ni)
    V = FunctionSpace(mesh, "Lagrange", el_order)
    print 'FEniCS running with ', V.dim(), ' degrees of freedom'
    if counter>0:
        dof.append(V.dim())

    # Define Dirichlet boundary (x = 0 or x = 1)
    def boundary(x, on_boundary):
        return on_boundary

    # Define boundary condition
    u0 = Constant(0.0)
    bc = DirichletBC(V, u0, boundary)

    # Define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)
    f = Expression("2*cos(x[0])*cos(x[1])")

    a = inner(nabla_grad(u), nabla_grad(v))*dx
    L = f*v*dx

    # assembly
    t1 = time()
    A = assemble(a)
    b = assemble(L)
    bc.apply(A, b)
    t2 = time()
    if counter>0:
        assemblytime.append(t2-t1)

    # Compute solution
    t1 = time()
    u = Function(V)
    U = u.vector()
    solve(A, U, b)
    t2 = time()
    if counter>0:
        solvetime.append(t2-t1)
    counter += 1
    # err = np.max( np.abs( u.compute_vertex_values()-manusol_fn(mesh.coordinates())))
    
    # # Explicit interpolation of u_e to higher-order elements,
    # # u will also be interpolated to the space Ve before integration
    # Ve = FunctionSpace(mesh, 'Lagrange', degree=5)
    # u_e_Ve = interpolate(manu_ex, Ve)
    # error = (u - u_e_Ve)**2*dx
    # E1 = sqrt(assemble(error))

    # errs.append(err)
    # errs_int.append(E1)

# dof = np.array(dof)
# errs = np.array(errs)
# errs_int = np.array(errs_int)

np.savetxt('fenics_dof.txt', dof)
np.savetxt('fenics_assembly.txt', assemblytime)
np.savetxt('fenics_solve.txt', solvetime)

# ## plot
# plt.figure()
# plt.loglog(dof, assemblytime , '.-')
# plt.loglog(dof, solvetime , '.-')
# plt.legend(['assembly', 'solve'])
# plt.ylabel('wall clock time (s)')
# plt.xlabel('DOF')
# plt.show()

# ## convergence:
# print 'Convergence rate:', np.median(np.diff(np.log10(errs))/np.diff(np.log10(dof)))
# print 'Convergence rate:', np.median(np.diff(np.log10(errs_int))/np.diff(np.log10(dof)))
# plt.loglog(dof, errs, '.-')
# plt.loglog(dof, errs_int, 'r.-')
# plt.xlabel('DOF')
# plt.ylabel('max err')
