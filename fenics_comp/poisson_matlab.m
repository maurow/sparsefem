% This times the Poisson equation in 2D for comparison with FEniCS.  See
% ../examples/poisson.m for details.

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

addpath ..
addpath ../../mattri

%% Manufactured solution
manusol = @(x,y) cos(x).*cos(y);
% forcing which produces the manufactured solution
force = @(x,y) 2*cos(x).*cos(y); 

% number of nodes in x & y-direction:
ns = 2.^(1:10)+1;

norminf = [];
n_nodes = [];
assemblytime = [];
setuptime = [];
solvetime = [];
count = 0;
% run with different mesh sizes
for n=ns([1,1:end]) % matlab needs one loop to warm up, presumably the JIT
    %% 2D mesh
    nx = n; ny = n;
    bmark = ones(4,1); bmark_edge = ones(4,1);
    mesh = make_2D_boxmesh(nx, ny, [-pi,pi]/2, [-pi,pi]/2, bmark, bmark_edge);
    
    %% FEM
    k = 1;
    solm = manusol(mesh.nodes(:,1),mesh.nodes(:,2));
    f = force(mesh.nodes(:,1),mesh.nodes(:,2));

    %% FEM setup
    tic
    [int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en, ...
     int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy] = FEoperators(mesh);

    % active nodes, i.e. not Dirichlet nodes
    anodes = mod(mesh.bmark,2)==0; 
    dirinodes = ~anodes;
    %number of boundary faces with Neumann BCs: bmark_facet is even
    neumann_facets = mesh.bmark_facet>0 & mod(mesh.bmark_facet,2)==0; 

    % BC
    diri = 0;
    phi = zeros(mesh.n_nodes,1);
    phi(dirinodes) = diri;
    neumann = 0* ones(sum(neumann_facets),1);
    setup = toc;
    
    %% assembly
    tic
    % do BCs by adding contribution of Dirichlet nodes to rhs (forcing):
    lhs =   Dx_en(:,anodes).'*int_ee* k *Dx_en(:,anodes) ...
            + Dy_en(:,anodes).'*int_ee* k *Dy_en(:,anodes);
    rhs = int_nn(anodes,:) * f + int_nf_bdy(anodes,:) * neumann ...
          - Dx_en(:,anodes).'*int_ee* k *(Dx_en(:,dirinodes)*phi(dirinodes))...
          - Dy_en(:,anodes).'*int_ee* k *(Dy_en(:,dirinodes)*phi(dirinodes));
    assembly = toc;

    %% solve
    tic
    phi(anodes) = lhs\rhs;
    solve = toc;

    if count>0
        setuptime(end+1) = setup;
        solvetime(end+1) = solve;
        assemblytime(end+1) = assembly;
        norminf(end+1) = norm(solm-phi, Inf);
        n_nodes(end+1) = mesh.n_nodes;
    end
    count = count + 1;
end
%% the results

slope_assemblytime = median(diff(log10(assemblytime))./diff(log10(ns)));
slope_solvetime   = median(diff(log10(solvetime))./diff(log10(ns)));
slope_setuptime   = median(diff(log10(setuptime))./diff(log10(ns)));

% $$$ figure()
% $$$ loglog(ns, assemblytime , '.-')
% $$$ hold
% $$$ loglog(ns, solvetime , 'g.-')
% $$$ loglog(ns, setuptime , 'r.-')
% $$$ legend('assembly', 'solve', 'setup')
% $$$ ylabel('wall clock time (s)')
% $$$ xlabel('sqrt(DOF)')

