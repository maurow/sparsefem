function mesh = make_1D_mesh(a, b, n, rand_fac)
%  mesh = make_1D_mesh(a, b, n, rand_fac)
%
% Makes a 1D mesh structure containing all the necessary fields.  With bmark and
% bmark_facet == 1 at boundary.
%
% 1 argument call: argument are the vertex positions
%
% 2 or 3 argument call: 
%  - a: start of interval
%  - b: end
%  - n: number of points (100 default)
%  - rand_fac: adds a random offset to each node coordinate

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if nargin==1
    nodes = a;
else
    if ~exist('a', 'var') || isempty(a)
        a=0;
    end
    if ~exist('b', 'var') || isempty(b)
        b=1;
    end
    if ~exist('n', 'var') || isempty(n)
        n=100;
    end
    nodes = linspace(a,b,n)';
end
if ~exist('rand_fac', 'var') || isempty(rand_fac)
    rand_fac = 0;
else
    if rand_fac>=0.5
        error('rand_fac needs be smaller than 0.5')
    end
end
n = length(nodes);
mesh.n_nodes = n;
mesh.n_elements = n-1;
mesh.n_facets = n-1;

% randomize:
dx = nodes(2)-nodes(1);
nodes = nodes + rand(n,1)*rand_fac*dx;   

mesh.connect = [(1:n-1)', (2:n)'];
mesh.connect_facet = [[-1, 1:mesh.n_elements]', [(1:mesh.n_elements),-1]'];
mesh.nodes = nodes;
mesh.bmark = 0 *nodes;
mesh.bmark(1) = 1;
mesh.bmark(end) = 1;

mesh.bmark_facet = 0*mesh.connect_facet(:,1);
mesh.bmark_facet(1) = 1;
mesh.bmark_facet(end) = 1;

disp(['Mesh with ', num2str(n), ' nodes.'])                 
