function mesh = make_2D_boxmesh(nx, ny, xrange, yrange, bmarkin, bmark_facetin, order_mesh);
%  mesh = make_2D_boxmesh(nx, ny, xrange, yrange, bmark, bmark_facet, order_mesh);
%
% Creates a structured triangular mesh on a rectangle.
% 
% bmark -- 4-element vector with boundary marks of the four corners, from (0,0) anti-clockwise, defaults to 1
% bmark_facet -- 4-element vector with boundary marks for the edges, from (0,0) anti-clockwise, defaults to 1

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('bmarkin', 'var') || isempty(bmarkin)
    bmarkin = ones(4,1);
end
if ~exist('bmark_facetin', 'var') || isempty(bmark_facetin)
    bmark_facetin = ones(4,1);
end
if ~exist('order_mesh', 'var') || isempty(order_mesh)
    order_mesh = false;  % orders the mesh, takes a long time
end

n_nodes = nx*ny;
n_elements = (nx-1)*(ny-1)*2;
n_facets = (nx-1)*ny + (ny-1)*nx + n_elements/2;
connect = zeros(n_elements, 3);
connect_facet = zeros(n_facets, 2);
bmark = zeros(n_nodes, 1);
bmark_facet = zeros(n_facets, 1);
nodes =  zeros(n_nodes, 2);


dx = diff(xrange)/(nx-1);
dy = diff(yrange)/(ny-1);

% construct the node coordinates
[X,Y] = meshgrid(xrange(1):dx:xrange(2), yrange(1):dy:yrange(2));
X=X';Y=Y';
nodes = [X(:), Y(:)];

%% construct the elements:
i = 1; % el index
n = 1; % node index
while true
    if mod(n, nx)==0 % end of row
        n=n+1;
        continue
    elseif n+nx > n_nodes % end of domain
        break
    else
        connect(i,:) = [n, n+1, n+nx];
        i=i+1;
        n=n+1;
    end
end
i = n_elements/2+1;
n = n_nodes; % node index
while true
    if mod(n-1, nx)==0 % end of row
        n=n-1;
        continue
    elseif n-nx < 1 % end of domain
        break
    else
        connect(i,:) = [n, n-1, n-nx];
        i=i+1;
        n=n-1;
    end
end
%% make all edges
connect_facet_dup = [connect(:, [1,2]); connect(:, [1,3]); connect(:, [2,3])];
connect_facet_dup = sort(connect_facet_dup, 2);
% [C,IA,IC] = UNIQUE(A,'rows') also returns index vectors IA and IC such
%                              that C = A(IA,:) and A = C(IC,:). 
[connect_facet, ~, IC] = unique(connect_facet_dup, 'rows');
% http://www.mathworks.co.uk/matlabcentral/newsreader/view_thread/314326
% N = HISTC(X,EDGES), for vector X, counts the number of values in X
%    that fall between the elements in the EDGES vector (which must contain
%    monotonically non-decreasing values).  N is a LENGTH(EDGES) vector
%    containing these counts. 
hi = histc(IC, 1:n_facets);
on_boundary = find(hi==1);
facet_midpoints = (nodes(connect_facet(:,1),:) + nodes(connect_facet(:,2),:))/2;
midp_on_b = facet_midpoints(on_boundary,:);

%% bmarks
small = min(dx,dy)/100;
bmark = zeros(n_nodes,1);
bmark_facet = zeros(n_facets,1);
% nodes made along edges
bmark(nodes(:,2)<yrange(1)+small) = bmark_facetin(1);
bmark(nodes(:,2)>yrange(2)-small) = bmark_facetin(3);
bmark(nodes(:,1)<xrange(1)+small) = bmark_facetin(4);
bmark(nodes(:,1)>xrange(2)-small) = bmark_facetin(2);
% four corners
bmark(1) = bmarkin(1);
bmark(nx) = bmarkin(2);
bmark(nx*ny) = bmarkin(3);
bmark(nx*ny-nx+1) = bmarkin(4);
% edges
bmark_facet(on_boundary(midp_on_b(:,2)<yrange(1)+small)) = bmark_facetin(1);
bmark_facet(on_boundary(midp_on_b(:,2)>yrange(2)-small)) = bmark_facetin(3);
bmark_facet(on_boundary(midp_on_b(:,1)<xrange(1)+small)) = bmark_facetin(4);
bmark_facet(on_boundary(midp_on_b(:,1)>xrange(2)-small)) = bmark_facetin(2);

if order_mesh
    %% order mesh: takes a long time!
    % this is copied from mattri: neighbour_node_matrix.m and order_mesh.m
    ce1 = connect_facet(:,1);
    ce2 = connect_facet(:,2);
    row = zeros(n_nodes*6,1);
    col = zeros(n_nodes*6,1);
    val = 1;
    counter = 1;
    for jj = 1:n_nodes
        tnodes1 = find(ce1==jj);
        tnodes2 = find(ce2==jj);    
        nadd = length(tnodes1)+length(tnodes2);
        row(counter:counter+nadd-1) = jj;
        col(counter:counter+nadd-1) = [ce2(tnodes1); ce1(tnodes2)];
        counter = counter + nadd;
    end
    row = row(1:counter-1);
    col = col(1:counter-1);
    neigh = sparse(row, col, val, n_nodes, n_nodes);
    % permutation to apply:
    per = symrcm(neigh);
    % inverse permutation http://blogs.mathworks.com/loren/2007/08/21/reversal-of-a-sort/
    tmp = 1:n_nodes;
    inv_per(per) = tmp;

    % do permutation
    nodes = nodes(per,:);
    bmark = bmark(per);
    % elements
    connect = inv_per(connect);
    connect_facet = inv_per(connect_facet);
end


%% fill struct
mesh.n_nodes = n_nodes;
mesh.n_elements = n_elements;
mesh.n_facets = n_facets;
mesh.connect = connect;
mesh.connect_facet = connect_facet;
mesh.bmark = bmark;
mesh.bmark_facet = bmark_facet;
mesh.nodes =  nodes;
% debug:
mesh.edge_midpoints = facet_midpoints;
mesh.connect_edge = connect_facet;
mesh.bmark_edge = bmark_facet;

disp(['Mesh with ', num2str(n_nodes), ' nodes.'])                 