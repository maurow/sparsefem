function [out, boundary_inds] = make_2D_mesh(boundary_xy, bmark, bmark_facets, maxarea, varargin)
%  [out, boundary_inds] = make_2D_mesh(boundary_xy, bmark, bmark_facets, maxarea, varargin)
%
% Makes a mesh using mattri and translates the fieldnames given in mattri mesher to more
% modern ones: edges --> facets

% This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
% Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

[mesh, boundary_inds] = make_mesh(boundary_xy, bmark, bmark_facets, maxarea, varargin{:});

% translate to new fieldnames:
fls = fieldnames(mesh.tri);
for ii = 1:length(fls)
    fl1 = fls{ii};
    if length(strfind(fl1, 'edges'))>0
        fl2 = strrep(fl1, 'edges', 'facets');
    elseif length(strfind(fl1, 'edge'))>0
        fl2 = strrep(fl1, 'edge', 'facet');        
    else
        fl2 = fl1;
    end
    out.(fl2) = mesh.tri.(fl1);
end

